# Group 5: Letter Recognition

## Team Members

- [4152] Hayden Kreuter (hkreuter@charlotte.edu) 
- [4152] Kai Reichow (kreichow@charlotte.edu)
- [5152] Sampada Sakpal (ssakpal@charlotte.edu)
- [5152] Nicole Wiktor (nwiktor@charlotte.edu)
- [4152] Franky Yang (lyang36@uncc.edu)

## Table of Contents

- [Final Report Jupyter Notebook](./Final_Report.ipynb)
- [Final Report PDF](./Final_Report.ipynb_-_Colaboratory.pdf)
- [Final Presentation](./Letter-Recognition-Final-Presentation.pdf)

## Usage

Please run the `Final Report` jupyter notebook step by step to train and execute the model.
